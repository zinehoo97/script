import os
import cv2
import numpy as np
import threading

def padding_resize(im, desired_size=128):
    old_size = im.shape[:2]  # old_size is in (height, width) format

    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    # new_size should be in (width, height) format

    im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    color = [0, 0, 0]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                value=color)
    return new_im


# l = os.listdir("/media/nhatdeptrai/DATA/up-s31/s31_foreground")
# count = 1
# for n in l:
#     image = cv2.imread("/media/nhatdeptrai/DATA/up-s31/s31_foreground/"+n)
#     ann = cv2.imread("/media/nhatdeptrai/DATA/up-s31/data_6_parts/ann/"+n.replace("image", "ann"))
#
#     y_nonzero, x_nonzero, _ = np.nonzero(image)
#     y_min = np.min(y_nonzero)
#     y_max = np.max(y_nonzero)
#     x_min = np.min(x_nonzero)
#     x_max = np.max(x_nonzero)
#
#     image = image[y_min:y_max, x_min:x_max]
#     ann = ann[y_min:y_max, x_min:x_max]
#
#     image = padding_resize(image, 256)
#     ann = padding_resize(ann, 256)
#
#
#     cv2.imwrite("/media/nhatdeptrai/DATA/up-s31/s31_data_padding_resize/images/"+n.replace("_image", ""), image)
#     cv2.imwrite("/media/nhatdeptrai/DATA/up-s31/s31_data_padding_resize/ann/"+n.replace("_ann", ""), ann)
#     print(count)
#     count+=1