import os
import cv2
import numpy as np

input_path = "/media/nhatdeptrai/DATA/data_test_skin_body/test_cut/"
segment_path = "/media/nhatdeptrai/DATA/data_test_skin_body/segment/"
list_image = os.listdir(input_path)

left_arm = (0, 255, 255)
right_arm = (51, 170, 221)
color = [left_arm, right_arm]

count = 1
for name in list_image:
    image = cv2.imread(input_path+name)
    segment = cv2.imread(segment_path+name)
    segment = cv2.cvtColor(segment, cv2.COLOR_BGR2RGB)
    new_img = image.copy()
    white_mask = np.ones(image.shape) * 255

    for c in color:
        mask = cv2.inRange(segment, c, c)
        mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2RGB)

        selected = [np.logical_and(mask!=[0,0,0], image==[0,0,0])]
        new_img[selected] = white_mask[selected]

    # new_img = cv2.cvtColor(new_img, cv2.COLOR_RGB2BGR)
    # cv2.imshow("hjhj", new_img)
    # cv2.waitKey()

    cv2.imwrite("/media/nhatdeptrai/DATA/data_test_skin_body/test_cut_white/"+name, new_img)
    print(count)
    count+=1
    
