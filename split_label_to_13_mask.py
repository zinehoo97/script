import cv2
import os
import numpy as np


bg = [0,0,0]
hair = [255,0,0]
upper_clothes = [255,85,0]
dress = [0,0,85]
pants = [0,85,85]
skirt = [0,128,0]
face = [0,0,255]
left_arm = [51,170,221]
right_arm = [0,255,255]
left_leg = [85,255,170]
right_leg = [170,255,85]
left_shoe = [255,255,0]
right_shoe = [255,170,0]


colors = [bg, hair, upper_clothes, dress, pants, skirt, face, left_arm, right_arm, left_leg, right_leg,
                   left_shoe, right_shoe]

upper = [upper_clothes, dress]
bottom = [pants, skirt]
l_arm = [left_arm]
r_arm = [right_arm]
l_leg = [left_leg, left_shoe]
r_leg = [right_leg, right_shoe]
face_ = [face, hair]

colors = [upper_clothes, pants, face, left_arm, right_arm, left_leg, right_leg]
list_segment = os.listdir("/media/nhatdeptrai/DATA/ATR_module2_crop/inputs_new_module2/segment_8_classes")

count = 1
for name in list_segment:
    path = "/media/nhatdeptrai/DATA/ATR_module2_crop/inputs_new_module2/segment_8_classes_mask/"+name.split(".")[0]+"/"
    os.mkdir(path)

    segment = cv2.imread("/media/nhatdeptrai/DATA/ATR_module2_crop/inputs_new_module2/segment_8_classes/"+name)
    segment = cv2.cvtColor(segment, cv2.COLOR_BGR2RGB)
    id = 0
    for c in colors:
        mask = cv2.inRange(segment, tuple(c), tuple(c))
        cv2.imwrite(path+str(id)+".png", mask)
        id += 1
    print(count)
    count+=1