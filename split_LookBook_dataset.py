import cv2
import os
import numpy as np
import threading


# Background  # Hair     # Face       # Tops      # Left-arm
SMEX_LABEL_COLOUR = [(0, 0, 0), (255, 0, 0), (0, 0, 255), (0, 0, 85), (0, 255, 255),
                     # Right-arm       # Bottoms       # Left-leg         # Right-leg
                     (51, 170, 221), (0, 85, 85), (170, 255, 85), (85, 255, 170)]

dataset_path = "/media/nhatdeptrai/DATA/lookbook/dataset_upper/"


def process(images, source):
    """
    :param images:
    :param source:
    :param destination:
    :param source_colors_to_classes: RGB to class indices
    :param target_colors: list of RGB tuples (each index corresponds to class index)
    :return:
    """

    count = 1

    for image in images:
        save_path = dataset_path + image.split(".")[0] + "/"

        img = cv2.imread("/media/nhatdeptrai/DATA/lookbook/data_resize/"+image.split(".")[0]+".jpg")
        segment = cv2.imread(os.path.join(source, image))
        segment = cv2.cvtColor(segment, cv2.COLOR_BGR2RGB)
        mask = cv2.inRange(segment, (0, 0, 85), (0, 0, 85))

        upper = img & cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)

        os.mkdir(save_path)
        cv2.imwrite(save_path+"upper_mask.png", mask)
        cv2.imwrite(save_path+"upper.jpg", upper)
        print("Process {}".format(count))
        count += 1


if __name__ == '__main__':
    source = '/media/nhatdeptrai/DATA/lookbook/segment_8_classes'

    images = [x for x in os.listdir(source) if "CLEAN1" not in x]

    threading.Thread(target=process, args=(images[0:20000], source,)).start()
    threading.Thread(target=process, args=(images[20000:40000], source,)).start()
    threading.Thread(target=process, args=(images[40000:60000], source,)).start()
    threading.Thread(target=process, args=(images[60000:len(images)], source,)).start()
