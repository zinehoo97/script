import os
import cv2


# local
label_path = "/media/nhatdeptrai/DATA/ATR_module2_crop/labels/"
segment_path = "/media/nhatdeptrai/DATA/ATR_module2_crop/inputs_new_module2/segment_8_classes_mask/"
body_path = "/media/nhatdeptrai/DATA/ATR_module2_crop/inputs_new_module2/6_body_bitwise/"
clothes_path = "/media/nhatdeptrai/DATA/ATR_module2_crop/inputs_new_module2/upper_bottom/"

# # cloud
# label_path = "/home/ubuntu/ATR_module2_crop/labels/"
# segment_path = "/home/ubuntu/ATR_module2_crop/clothes_segment_13_mask/"
# body_crop_path = "/home/ubuntu/ATR_module2_crop/body_crop/"


def padding_resize(im, desired_size=513):
    old_size = im.shape[:2]  # old_size is in (height, width) format

    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    # new_size should be in (width, height) format

    im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    color = [0, 0, 0]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                value=color)
    return new_im


def check_shape(img, path):
    if img.shape[0] != 256 or img.shape[1] != 256:
        print(path)
        new_im = padding_resize(img, 256)
        cv2.imwrite(path, new_im)
def _parse_function(image_name):
    label = cv2.imread(label_path+image_name+".jpg")
    check_shape(label, label_path+image_name+".jpg")

image_names = [x.split(".")[0] for x in os.listdir(label_path)]

for name in image_names:
    _parse_function(name)
