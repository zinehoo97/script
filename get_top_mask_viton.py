import cv2
import random
import os
import numpy as np

def padding_resize(im, desired_size=128):
    old_size = im.shape[:2]  # old_size is in (height, width) format

    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    # new_size should be in (width, height) format

    im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    color = [0, 0, 0]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                value=color)
    return new_im


image = cv2.imread("/home/nhatdeptrai/Desktop/sf.pix2pix_inception_dataset/Image/Image/Origin/000008_0.jpg")

mask_image = cv2.inRange(image, (0,0,0), (0,0,0))
kernel = np.ones((1, 1), np.uint8)
mask_image = cv2.erode(mask_image, kernel, iterations=1)

mask_image[mask_image==255] = 100
mask_image[mask_image==0] = 255
mask_image[mask_image==100] = 0
cv2.imshow("hjhj", image)
cv2.imshow("hjhjhj", mask_image)
cv2.waitKey()