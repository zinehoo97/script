import os
import cv2
import numpy as np
import random
import threading

def padding_resize(im, desired_size=128):
    old_size = im.shape[:2]  # old_size is in (height, width) format

    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    # new_size should be in (width, height) format

    im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    color = [0, 0, 0]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                value=color)
    return new_im


def random_transform(image, min_percentage, max_percentage):
    y_nonzero, x_nonzero, _ = np.nonzero(image)
    image_transform = np.zeros(image.shape)
    if x_nonzero.size != 0:
        x_min = np.min(x_nonzero)
        x_max = np.max(x_nonzero)
        y_min = np.min(y_nonzero)
        y_max = np.max(y_nonzero)

        left_bottom = [x_min, y_max]
        left_top = [x_min, y_min]
        right_top = [x_max, y_min]
        right_bottom = [x_max, y_max]

        x_avg = int((x_min + x_max) / 2)
        y_avg = int((y_min + y_max) / 2)

        points = np.array([left_bottom, left_top, right_top, right_bottom])

        dst_points = np.array([
            (left_bottom[0] + random.choice([-1, 1]) * random.randint(int(x_avg * min_percentage / 100),
                                                                      int(x_avg * max_percentage / 100)),
             left_bottom[1] + random.choice([-1, 1]) * random.randint(int(y_avg * min_percentage / 100),
                                                                      int(y_avg * max_percentage / 100))),
            (left_top[0] + random.choice([-1, 1]) * random.randint(int(x_avg * min_percentage / 100),
                                                                   int(x_avg * max_percentage / 100)),
             left_top[1] + random.choice([-1, 1]) * random.randint(int(y_avg * min_percentage / 100),
                                                                   int(y_avg * max_percentage / 100))),
            (right_top[0] + random.choice([-1, 1]) * random.randint(int(x_avg * min_percentage / 100),
                                                                    int(x_avg * max_percentage / 100)),
             right_top[1] + random.choice([-1, 1]) * random.randint(int(y_avg * min_percentage / 100),
                                                                    int(y_avg * max_percentage / 100))),
            (right_bottom[0] + random.choice([-1, 1]) * random.randint(int(x_avg * min_percentage / 100),
                                                                       int(x_avg * max_percentage / 100)),
             right_bottom[1] + random.choice([-1, 1]) * random.randint(int(y_avg * min_percentage / 100),
                                                                       int(y_avg * max_percentage / 100)))
        ])
        points = np.float32(points)
        dst_points = np.float32(dst_points)

        M = cv2.getPerspectiveTransform(points, dst_points)
        image_transform = cv2.warpPerspective(image, M, (image.shape[1], image.shape[0]))
    return image_transform

#local
image_path = "/home/nhatdeptrai/Desktop/humans/"
segment_path = "/home/nhatdeptrai/Desktop/uppers_and_people_schp_to_smex_color/"
mask_path = "/home/nhatdeptrai/Desktop/full_body_mask/"

image_list_segment = [x.split("_")[0] for x in os.listdir(segment_path)]
image_list_human = [x.split("_")[0] for x in os.listdir(image_path)]
image_list_mask = [x.split("_")[0] for x in os.listdir(mask_path)]

human_list = list((set(image_list_segment) & set(image_list_human)) & set(image_list_mask))

 #local
image_path = "/home/nhatdeptrai/Desktop/women_top/"
dst_path = "/home/nhatdeptrai/Desktop/viton_random_384_10/"

image_list = [x.split("_")[0] for x in os.listdir(image_path) if "_1" in x]

image_list = list(set(image_list) & set(human_list))
print(len(image_list))

def process(image_names):
    l = len(image_names)
    count = 0
    for name in image_names:
        image_raw = cv2.imread(image_path+name+"_1.jpg")
        mask_bg = cv2.inRange(image_raw, (255, 255, 255), (255, 255, 255))
        mask_bg = cv2.bitwise_not(mask_bg)
        image = cv2.cvtColor(mask_bg, cv2.COLOR_GRAY2BGR) & image_raw
        save_path = dst_path + name + "_1/"
        os.mkdir(save_path)
        for i in range(10):
            try:
                img_transform = random_transform(image, 10, 20)
                y_nonzero, x_nonzero, _ = np.nonzero(img_transform)
                y_min = np.min(y_nonzero)
                y_max = np.max(y_nonzero)
                x_min = np.min(x_nonzero)
                x_max = np.max(x_nonzero)

                img_transform = img_transform[y_min:y_max, x_min:x_max]

                img_transform = padding_resize(img_transform, 384)
                # body_non_shirt_gray = cv2.cvtColor(np.uint8(img_transform), cv2.COLOR_BGR2GRAY)
                # _, body_non_shirt_gray = cv2.threshold(np.uint8(body_non_shirt_gray), 1, 255, 0)
                # contours, hierarchy = cv2.findContours(body_non_shirt_gray, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
                # if len(contours) != 0:
                #     for contour in contours:
                #         if cv2.contourArea(contour) < 20:
                #             cv2.drawContours(img_transform, [contour], -1, (0, 0, 0), -1)

                # cv2.imshow("hjhjhj", img_transform)
                # cv2.imshow("hjhjhja", img_transform_256)
                # cv2.imshow("hjhjahj", img_transform_512)
                # cv2.waitKey()

                cv2.imwrite(save_path+str(i)+".jpg", img_transform)
            except:
                pass
        count+=1
        print("Da xu ly: "+str(count)+", tong so: "+str(l))


# process(image_list[0:20])
for i in range(0, 10001, 1000):
    threading.Thread(target=process, args=(image_list[i:i+1000], )).start()
# threading.Thread(target=process, args=(image_list[200:400], )).start()
# threading.Thread(target=process, args=(image_list[4:6], )).start()
# threading.Thread(target=process, args=(image_list[6:8], )).start()
# threading.Thread(target=process, args=(image_list[8:10], )).start()
# threading.Thread(target=process, args=(image_list[10:12], )).start()
# threading.Thread(target=process, args=(image_list[12:14], )).start()
# threading.Thread(target=process, args=(image_list[14:16], )).start()
# threading.Thread(target=process, args=(image_list[16:18], )).start()
# threading.Thread(target=process, args=(image_list[18:20], )).start()
# threading.Thread(target=process, args=(image_list[4000:4400], )).start()
# threading.Thread(target=process, args=(image_list[4400:4800], )).start()
# threading.Thread(target=process, args=(image_list[4800:5200], )).start()
# threading.Thread(target=process, args=(image_list[5200:5600], )).start()
# threading.Thread(target=process, args=(image_list[5600:6000], )).start()
# threading.Thread(target=process, args=(image_list[6000:6400], )).start()
# threading.Thread(target=process, args=(image_list[6400:6800], )).start()
# threading.Thread(target=process, args=(image_list[6800:7200], )).start()
# threading.Thread(target=process, args=(image_list[7200:7600], )).start()
# threading.Thread(target=process, args=(image_list[7600:8000], )).start()
# threading.Thread(target=process, args=(image_list[8000:8400], )).start()
# threading.Thread(target=process, args=(image_list[8400:8800], )).start()
# threading.Thread(target=process, args=(image_list[8800:9200], )).start()
# threading.Thread(target=process, args=(image_list[9200:9600], )).start()
# threading.Thread(target=process, args=(image_list[9600:10000], )).start()
# threading.Thread(target=process, args=(image_list[10000:len(image_list)], )).start()
