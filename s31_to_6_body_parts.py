import os
import cv2
import numpy as np
import random
from padding_resize import padding_resize

path = "/media/nhatdeptrai/DATA/up-s31/s31_foreground/"
l = os.listdir(path)

left_arm = [1, 2, 3, 4, 5, 6]
right_arm = [14, 15, 16, 17, 18, 19]
trunk = [7, 8, 20, 21]
left_leg = [9, 10, 11, 12, 13]
right_leg = [22, 23, 24, 25, 26]
head = [27, 28, 29, 30, 31]

id_part = [left_arm, right_arm, trunk, left_leg, right_leg, head]

# #Color in RGB order
# left_arm_color = [255, 215, 50]
# right_arm_color = [85, 155, 45]
# trunk_color = [170, 35, 170]
# left_leg_color = [35, 70, 170]
# right_leg_color = [100, 175, 165]
# head_color = [200, 35, 135]
#
# color = [left_arm_color, right_arm_color, trunk_color, left_leg_color, right_leg_color, head_color]
count = 1

for n in l:
    # if "ann." in n:
    count+=1
    image = cv2.imread(path+n)
    label = cv2.imread("/media/nhatdeptrai/DATA/up-s31/s31/"+n.replace("image", "ann"))
    # vis = np.zeros(label.shape)
    gray_img = cv2.cvtColor(label, cv2.COLOR_BGR2GRAY)
    gray_new = np.zeros(gray_img.shape)
    for i in range(len(id_part)):
        # color_mask = np.ones(label.shape)
        # color_mask = np.where(color_mask[...,[0]]==1,color[i],color_mask)
        for id in id_part[i]:
            # print(id)
            # cv2.imshow("hjhj", gray_img)
            # cv2.waitKey()
            # print(gray_img)
            # print(gray_img==id)
            #vis[gray_img==id] = color_mask[gray_img==id]
            gray_new[gray_img==id] = i+1

    y_crop = random.randint((513*3)//5, (513*5)//7)

    image_crop = image.copy()[0:y_crop, 0:gray_new.shape[1]]
    label_crop = gray_new.copy()[0:y_crop, 0:gray_new.shape[1]]

    image_crop = padding_resize(image_crop, 513)
    label_crop = padding_resize(label_crop, 513)
    image = padding_resize(image, 513)
    label = padding_resize(gray_new, 513)

    new_name = n.split("_")[0] +".png"
    new_name_crop = n.split("_")[0] +"_crop.png"

    cv2.imshow("/media/nhatdeptrai/DATA/up-s31/up_s31_clean/images/" + new_name_crop, image_crop)
    cv2.imshow("/media/nhatdeptrai/DATA/up-s31/up_s31_clean/images/" + new_name, image)
    cv2.imshow("/media/nhatdeptrai/DATA/up-s31/up_s31_clean/ann/" + new_name_crop, label_crop)
    cv2.imshow("/media/nhatdeptrai/DATA/up-s31/up_s31_clean/ann/" + new_name, label)
    cv2.waitKey()

    # cv2.imwrite("/media/nhatdeptrai/DATA/up-s31/up_s31_clean/images/"+new_name_crop, image_crop)
    # cv2.imwrite("/media/nhatdeptrai/DATA/up-s31/up_s31_clean/images/"+new_name, image)
    # cv2.imwrite("/media/nhatdeptrai/DATA/up-s31/up_s31_clean/ann/"+new_name_crop, label_crop)
    # cv2.imwrite("/media/nhatdeptrai/DATA/up-s31/up_s31_clean/ann/"+new_name, label)
    # cv2.waitKey()
    print(count)

    # print(vis)
    # new_name = n.split("_")[0] +".png"
    #wild_image = cv2.imread(path+n.split("_")[0]+"_image.png")
    #cv2.imwrite("/media/nhatdeptrai/DATA/up-s31/data_6_parts/images/"+n, wild_image)
    #cv2.imwrite("/media/nhatdeptrai/DATA/up-s31/data_6_parts/segments/"+n, vis)
    # cv2.imwrite("/media/nhatdeptrai/DATA/up-s31/data_6_parts/ann/"+n, gray_new)