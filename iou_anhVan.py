import os
import cv2
import numpy as np
from padding_resize import padding_resize
# colour map
label_colours = [[0, 0, 0], [255, 215, 50], [85, 155, 45], [170, 35, 170], [35, 70, 170], [100, 175, 165], [200, 35, 135]]


def mean_iou(labels, predictions, n_classes):
    mean_iou = 0.0
    seen_classes = 0

    for c in range(n_classes):
        labels_c = (labels == c)
        pred_c = (predictions == c)

        intersect = labels_c & pred_c
        union = labels_c | pred_c

        intersect_sum = (intersect).sum()
        union_sum = (union).sum()

        if (intersect_sum > 0) or (union_sum > 0):
            seen_classes += 1
            mean_iou += intersect_sum / union_sum

    return mean_iou / seen_classes if seen_classes else 0



l = os.listdir("/media/nhatdeptrai/DATA/up-s31/test_anhVan/label/mask_body_part")
sum_iou = 0
for n in l:
    predict = cv2.imread("/media/nhatdeptrai/DATA/up-s31/test_anhVan/predict/"+n.split(".")[0]+"_mask.png")
    label = cv2.imread("/media/nhatdeptrai/DATA/up-s31/test_anhVan/label/mask_body_part/"+n)
    label = padding_resize(label, 256)
    predict_mask = np.zeros((predict.shape[0], predict.shape[1], predict.shape[2]))
    for i in range(len(label_colours)):
        indices = np.where(predict == tuple(label_colours[i]))
        predict_mask[indices] = (np.ones(predict_mask.shape)*i)[indices]

    # predict_mask = cv2.cvtColor(np.uint8(predict_mask), cv2.COLOR_BGR2GRAY)
    # label = cv2.cvtColor(label, cv2.COLOR_BGR2GRAY)
    sum_iou += (mean_iou(label, predict_mask, 7))
    # cv2.imshow("asd", label)
    # cv2.imshow("asda", np.uint8(predict_mask))
    # cv2.waitKey()
print(sum_iou / len(l))