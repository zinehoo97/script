import  cv2
import numpy as np

face = (0,0,255)
left_arm = (0, 255,255)
right_arm = (51,170,221)
left_leg = (170,255,85)
right_leg = (85,255,170)

color = [left_arm, right_arm]

def padding_resize(im, desired_size):
    old_size = im.shape[:2]  # old_size is in (height, width) format
    old_size_keeping = (im.shape[1], im.shape[0])
    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    # new_size should be in (width, height) format

    im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    color = [0, 0, 0]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                value=color)
    return new_im, old_size_keeping

def recovery_size(image, old_size):
    mask = image > 0
    coords = np.argwhere(mask)

    x0, y0, _ = coords.min(axis=0)
    x1, y1, _ = coords.max(axis=0) + 1

    cropped = image[x0:x1, y0:y1]
    cropped_resize = cv2.resize(cropped, old_size)

    return cropped_resize


image = cv2.imread("/home/nhatdeptrai/Desktop/Script/anh19.jpg")
segment = cv2.imread("/home/nhatdeptrai/Desktop/Script/anh19_vis.png")
segment = cv2.cvtColor(segment, cv2.COLOR_BGR2RGB)

for i in range(len(color)):
    left_arm_mask = cv2.inRange(segment, color[i], color[i])
    y_zeros, x_zeros = np.nonzero(left_arm_mask)

    if x_zeros.size != 0:
        x_min = np.min(x_zeros)
        y_min = np.min(y_zeros)
        x_max = np.max(x_zeros)
        y_max = np.max(y_zeros)

        dx = x_max - x_min
        dy = y_max - y_min

        rate = 0.2

        crop_image = image[y_min-int(dy*rate): y_max+int(dy*rate), x_min-int(dx*rate):x_max+int(dx*rate)]
        crop_segment = segment[y_min - int(dy * rate): y_max + int(dy * rate), x_min - int(dx * rate): x_max + int(dx * rate)]
        crop_segment, old_shape = padding_resize(crop_segment, 128)
        crop_segment_recovered = recovery_size(crop_segment, old_shape)

        image[y_min-int(dy*rate): y_max+int(dy*rate), x_min-int(dx*rate):x_max+int(dx*rate)] = crop_segment_recovered
        cv2.imshow("hjhj", image)
        cv2.waitKey()