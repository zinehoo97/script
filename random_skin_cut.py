import cv2
import random
import os
import numpy as np
from skimage.draw import random_shapes

# Background  # Hair     # Face       # Tops      # Left-arm
background = [0, 0, 0]
hair = [255, 0, 0]
face = [0, 0, 255]
top = [0, 0, 85]
left_arm = [0, 255, 255]
right_arm = [51, 170, 221]
bottom = [0, 85, 85]
left_leg = [170, 255, 85]
right_leg = [85, 255, 170]

color = [left_arm, right_arm]

def padding_resize(im, desired_size=128):
    old_size = im.shape[:2]  # old_size is in (height, width) format

    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    # new_size should be in (width, height) format

    im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    color = [0, 0, 0]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                value=color)
    return new_im

def random_skin_cut(ori_img, part_body, pts):
    part_ = part_body.copy()
    ori = ori_img.copy()
    mask = np.zeros((part_.shape[0], part_.shape[1]))
    for p in pts:
        part = part_[p[0][1]:p[1][1], p[0][0]:p[1][0]]
        try:
            s = random.choice(["rectangle", "circle"])
            size_min = 15
            random_mask, _ = random_shapes((part.shape[0], part.shape[1]),shape=s , min_size=min(size_min, min(part.shape[0], part.shape[1])), max_shapes=10)
            random_mask = cv2.cvtColor(random_mask, cv2.COLOR_BGR2GRAY)
            _,random_mask = cv2.threshold(random_mask, 254, 255, type=cv2.THRESH_BINARY_INV)
            mask[p[0][1]:p[1][1], p[0][0]:p[1][0]] = random_mask
        except:
            pass
    part_ = cv2.cvtColor(part_, cv2.COLOR_BGR2GRAY)
    _, part_binary = cv2.threshold(part_, 1, 255, type=cv2.THRESH_BINARY)

    final_mask = part_binary.astype(np.uint8) & mask.astype(np.uint8)

    final_mask[final_mask==255] = 100
    final_mask[final_mask==0] = 255
    final_mask[final_mask==100] = 0

    final_mask = cv2.cvtColor(final_mask, cv2.COLOR_GRAY2BGR)

    ori[final_mask==[0,0,0]] = final_mask[final_mask==[0,0,0]]

    # cv2.imshow("as1dads", ori)
    # cv2.imshow("asd23eads", final_mask)
    # cv2.imshow("asdads", mask)
    # cv2.waitKey()
    # rs =
    return ori

segment_path = "/home/nhatdeptrai/Desktop/uppers_and_people_schp_to_smex_color/"
img_path = "/home/nhatdeptrai/Desktop/data_face_shirt_384/"
list_segment = [x.split(".")[0] for x in os.listdir(segment_path)]
list_img = [x.split(".")[0] for x in os.listdir(img_path)]
list_name = list(set(list_segment) & set(list_img))
print(len(list_name))
count = 0
for name in list_name:
    # path_n = "/home/nhatdeptrai/Desktop/data_skin_body_background_labels/random_input/"+name.split(".")[0]+"/"
    # os.mkdir(path_n)
    ori_img = cv2.imread(img_path + name + "/non_top_head.jpg")
    y_non, x_non, _ = np.nonzero(ori_img)
    if y_non.size != 0:
        y_min = np.min(y_non)
        diff = random.randint(10,30)
        ori_img[y_min: y_min+diff, 0:ori_img.shape[1]] = [0, 0, 0]

    segment = cv2.imread(segment_path+name+".png")
    segment = padding_resize(segment, 384)
    segment = cv2.cvtColor(segment, cv2.COLOR_BGR2RGB)

    mask_left = cv2.inRange(segment, tuple(left_arm), tuple(left_arm))
    mask_right = cv2.inRange(segment, tuple(right_arm), tuple(right_arm))
    mask_arm = mask_left | mask_right
    part = ori_img & cv2.cvtColor(mask_arm, cv2.COLOR_GRAY2BGR)

    pts = []

    for i in range(len(color)):
        mask = cv2.inRange(segment, tuple(color[i]), tuple(color[i]))
        y_non, x_non = np.nonzero(mask)
        if x_non.size != 0:
            x_max = np.max(x_non)
            x_min = np.min(x_non)
            y_min = np.min(y_non)
            y_max = np.max(y_non)
            dy = y_max - y_min
            dx = x_max - x_min
            p1 = (x_min, y_min)
            p2 = (x_max, y_max)
            pts.append([p1,p2])

    # for num in range(200):
    random_rs = random_skin_cut(ori_img, part, pts)
    cv2.imwrite("/home/nhatdeptrai/Desktop/random_skin_cut_viton_384/"+name+".jpg", random_rs)
    count +=1
    print(count)