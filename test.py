import cv2
import numpy as np
import os

# def padding_resize(im, desired_size=128):
#     old_size = im.shape[:2]  # old_size is in (height, width) format
#
#     ratio = float(desired_size) / max(old_size)
#     new_size = tuple([int(x * ratio) for x in old_size])
#
#     # new_size should be in (width, height) format
#
#     im = cv2.resize(im, (new_size[1], new_size[0]))
#
#     delta_w = desired_size - new_size[1]
#     delta_h = desired_size - new_size[0]
#     top, bottom = delta_h // 2, delta_h - (delta_h // 2)
#     left, right = delta_w // 2, delta_w - (delta_w // 2)
#
#     color = [0, 0, 0]
#     new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
#                                 value=color)
#     return new_im
#
#
# image = cv2.imread("/home/nhatdeptrai/Desktop/staff/500_staff/images/anh419.jpg")
# segment = cv2.imread("/home/nhatdeptrai/Desktop/staff/staff_shcp_to_smex_original/anh419.png")
#
# image = padding_resize(image, 800)
# segment = padding_resize(segment, 800)
#
# y_nonzero, x_nonzero, _ = np.nonzero(segment)
# y_min = np.min(y_nonzero)
# y_max = np.max(y_nonzero)
# x_min = np.min(x_nonzero)
# x_max = np.max(x_nonzero)
#
# image = image[y_min:y_max*2//3, x_min-40:x_max+40]
# segment = segment[y_min:y_max*2//3, x_min-40:x_max+40]
# cv2.imwrite("staff2.jpg", image)
# cv2.imwrite("staff2_segment.png", segment)


clean_list = [x.split("_")[0] for x in os.listdir("/home/nhatdeptrai/Desktop/viton_origin_on_store")]
image_list = [x.split("_")[0] for x in os.listdir("/home/nhatdeptrai/Desktop/viton_random_384_10")]

rm = list(set(image_list) - set(clean_list) & set(image_list))

import shutil
for name in rm:
    # os.remove("/home/nhatdeptrai/Desktop/random_skin_cut_viton_384/"+name+"_0.jpg")
    shutil.rmtree("/home/nhatdeptrai/Desktop/viton_random_384_10/"+name+"_1")