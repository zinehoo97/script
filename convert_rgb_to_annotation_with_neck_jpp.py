import cv2
import os
import numpy as np

bodypart_path = "/media/nhatdeptrai/DATA/VITON/body_parts_segment/"
segment_path = "/media/nhatdeptrai/DATA/VITON/jppnet_clothes_segment/"

bodypart_list = os.listdir(bodypart_path)
segment_list = os.listdir(segment_path)

bg = [[0,0,0]]
hair = [[255,0,0]]
upper_clothes = [[255,85,0], [0,119,221]]
dress = [[0,0,85], [85,51,0]]
pants = [[0,85,85]]
skirt = [[0,128,0]]
face = [[0,0,255], [170,0,51]]
left_arm = [[51,170,221]]
right_arm = [[0,255,255]]
left_leg = [[85,255,170]]
right_leg = [[170,255,85]]
left_shoe = [[255,255,0]]
right_shoe = [[255,170,0]]

colors = [bg,
hair,
upper_clothes,
dress,
pants,
skirt,
face,
left_arm,
right_arm,
left_leg,
right_leg,
left_shoe,
right_shoe]

# JPP_COLOR_MAP = [(0, 0, 0)
#                  # 0=Background
#                 , (128,0,0), (255,0,0), (0,85,0), (170,0,51), (255,85,0)
#                  # 1=Hat,  2=Hair,    3=Glove, 4=Sunglasses, 5=UpperClothes
#                 , (0,0,85), (0,119,221), (85,85,0), (0,85,85), (85,51,0)
#                  # 6=Dress, 7=Coat, 8=Socks, 9=Pants, 10=Jumpsuits
#                 , (52,86,128), (0,128,0), (0,0,255), (51,170,221), (0,255,255)
#                  # 11=Scarf, 12=Skirt, 13=Face, 14=LeftArm, 15=RightArm
#                 , (85,255,170), (170,255,85), (255,255,0), (255,170,0)]
#                 # 16=LeftLeg, 17=RightLeg, 18=LeftShoe, 19=RightShoe
count = 1
for img_name in bodypart_list:
    bodypart = cv2.imread(bodypart_path+img_name)
    segment = cv2.imread(segment_path+img_name)
    cv2.imshow("hjhj", segment)
    cv2.waitKey()
    # segment = cv2.cvtColor(segment, cv2.COLOR_BGR2RGB)
    #
    # result = np.zeros((segment.shape[0], segment.shape[1]))
    # for id in range(len(colors)):
    #     for c in colors[id]:
    #         mask_id = np.ones(result.shape)*id
    #         mask_color = cv2.inRange(segment, tuple(c), tuple(c))
    #         result[mask_color!=0] = mask_id[mask_color!=0]
    # cv2.imwrite("/media/nhatdeptrai/DATA/VITON/anno/"+img_name, result)
    # print(count)
    # count+=1
    # cv2.imshow("hjhj",cv2.imread("hjhj.png"))
    # cv2.waitKey()
