import os
import cv2
import numpy as np
import random
import threading

def padding_resize(im, desired_size=128):
    old_size = im.shape[:2]  # old_size is in (height, width) format

    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    # new_size should be in (width, height) format

    im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    color = [0, 0, 0]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                value=color)
    return new_im


def random_transform(image, min_percentage, max_percentage):
    y_nonzero, x_nonzero, _ = np.nonzero(image)
    image_transform = np.zeros(image.shape)
    if x_nonzero.size != 0:
        x_min = np.min(x_nonzero)
        x_max = np.max(x_nonzero)
        y_min = np.min(y_nonzero)
        y_max = np.max(y_nonzero)

        left_bottom = [x_min, y_max]
        left_top = [x_min, y_min]
        right_top = [x_max, y_min]
        right_bottom = [x_max, y_max]

        x_avg = int((x_min + x_max) / 2)
        y_avg = int((y_min + y_max) / 2)

        points = np.array([left_bottom, left_top, right_top, right_bottom])

        dst_points = np.array([
            (left_bottom[0] + random.choice([-1, 1]) * random.randint(int(x_avg * min_percentage / 100),
                                                                      int(x_avg * max_percentage / 100)),
             left_bottom[1] + random.choice([-1, 1]) * random.randint(int(y_avg * min_percentage / 100),
                                                                      int(y_avg * max_percentage / 100))),
            (left_top[0] + random.choice([-1, 1]) * random.randint(int(x_avg * min_percentage / 100),
                                                                   int(x_avg * max_percentage / 100)),
             left_top[1] + random.choice([-1, 1]) * random.randint(int(y_avg * min_percentage / 100),
                                                                   int(y_avg * max_percentage / 100))),
            (right_top[0] + random.choice([-1, 1]) * random.randint(int(x_avg * min_percentage / 100),
                                                                    int(x_avg * max_percentage / 100)),
             right_top[1] + random.choice([-1, 1]) * random.randint(int(y_avg * min_percentage / 100),
                                                                    int(y_avg * max_percentage / 100))),
            (right_bottom[0] + random.choice([-1, 1]) * random.randint(int(x_avg * min_percentage / 100),
                                                                       int(x_avg * max_percentage / 100)),
             right_bottom[1] + random.choice([-1, 1]) * random.randint(int(y_avg * min_percentage / 100),
                                                                       int(y_avg * max_percentage / 100)))
        ])
        points = np.float32(points)
        dst_points = np.float32(dst_points)

        M = cv2.getPerspectiveTransform(points, dst_points)
        image_transform = cv2.warpPerspective(image, M, (image.shape[1], image.shape[0]))
    return image_transform

#local
image_path = "/home/nhatdeptrai/Desktop/humans/"
segment_path = "/home/nhatdeptrai/Desktop/uppers_and_people_schp_to_smex_color/"
mask_path = "/home/nhatdeptrai/Desktop/full_body_mask/"
dst_data_face_shirt_path = "/home/nhatdeptrai/Desktop/data_face_shirt_384/"


# #cloud
# image_path = "/home/nhathl/humans/"
# segment_path = "/home/nhathl/uppers_and_people_schp_to_smex_color/"
# mask_path = "/home/nhathl/full_body_mask/"
#
# dst_path = "/home/nhathl/viton_random/"
# dst_ori_shirt_path = "/home/nhathl/viton_ori_top/"
# dst_data_face_shirt_path = "/home/nhathl/data_face_shirt/"

image_list_segment = [x.split(".")[0] for x in os.listdir(segment_path)]
image_list_human = [x.split(".")[0] for x in os.listdir(image_path)]
image_list_mask = [x.split(".")[0] for x in os.listdir(mask_path)]

image_list = list((set(image_list_segment) & set(image_list_human)) & set(image_list_mask))
print(len(image_list))

# image_list = os.listdir("/home/nhatdeptrai/Desktop/viton_random_non_human_384/")


hair = (255, 0, 0)
face = (0, 0, 255)

def process(image_names):
    l = len(image_names)
    count = 0
    for name in image_names:
        name = name.replace("_1", "_0")
        image = cv2.imread(image_path+name+".jpg")

        segment = cv2.imread(segment_path+name+".png")
        segment = cv2.cvtColor(segment, cv2.COLOR_BGR2RGB)

        segment_body = padding_resize(segment.copy(), 384)
        # cv2.imshow("segment", segment)
        image_body = padding_resize(image.copy(), 384)

        segment_gray = cv2.cvtColor(segment_body, cv2.COLOR_BGR2GRAY)
        mask = np.zeros(segment_gray.shape)
        mask[segment_gray>0] = 255
        contour, hier = cv2.findContours(np.uint8(mask), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

        for cnt in contour:
            if cv2.contourArea(cnt) < 100:
                cv2.drawContours(mask, [cnt], 0, 255, -1)
        # cv2.imshow("mask", mask)
        mask_bg = cv2.imread(mask_path+name+".jpg")
        mask_bg = cv2.cvtColor(mask_bg, cv2.COLOR_BGR2GRAY)
        mask_bg = cv2.resize(mask_bg, (762, 1100))
        mask_bg = padding_resize(mask_bg,  mask.shape[0])

        mask = np.uint8(mask) & np.uint8(mask_bg)

        image_body = cv2.cvtColor(np.uint8(mask), cv2.COLOR_GRAY2BGR) & image_body

        mask_face = cv2.inRange(segment_body, face, face)
        mask_hair = cv2.inRange(segment_body, hair, hair)
        mask_face_hair = mask_face | mask_hair

        body_old_shirt_mask = np.ones(mask_face_hair.shape)*255
        body_old_shirt_mask[mask_face_hair==255] = 0

        body_old_shirt = cv2.cvtColor(np.uint8(body_old_shirt_mask), cv2.COLOR_GRAY2BGR) & image_body
        # kernel = np.ones((2, 2), np.uint8)
        # body_old_shirt = cv2.erode(body_old_shirt, kernel, iterations=1)

        face_hair = cv2.cvtColor(mask_face_hair, cv2.COLOR_GRAY2BGR) & image_body
        # cv2.imshow("identify", face_hair)
        # cv2.imshow("label", body_old_shirt)

        mask_body = cv2.inRange(segment_body, (0, 0, 85), (0, 0, 85))
        body_non_shirt_mask = np.ones(mask_body.shape) * 255
        body_non_shirt_mask[mask_body == 255] = 0
        body_non_shirt = cv2.cvtColor(np.uint8(body_non_shirt_mask), cv2.COLOR_GRAY2BGR) & body_old_shirt

        body_non_shirt_gray = cv2.cvtColor(np.uint8(body_non_shirt), cv2.COLOR_BGR2GRAY)
        _, body_non_shirt_gray = cv2.threshold(np.uint8(body_non_shirt_gray), 1, 255, 0)
        contours, hierarchy = cv2.findContours(body_non_shirt_gray, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        if len(contours) != 0:
            for contour in contours:
                if cv2.contourArea(contour) < 10:
                    cv2.drawContours(body_non_shirt, [contour], -1, (0,0,0), -1)
                    cv2.drawContours(body_old_shirt, [contour], -1, (0,0,0), -1)
        # body_non_shirt = cv2.erode(body_non_shirt, kernel, iterations=1)


        # cv2.imshow("input", body_non_shirt)

        # mask = cv2.inRange(segment, (0, 0, 85), (0, 0, 85))
        # y_nonzero, x_nonzero = np.nonzero(mask)
        # y_min = np.min(y_nonzero)
        # y_max = np.max(y_nonzero)
        # x_min = np.min(x_nonzero)
        # x_max = np.max(x_nonzero)

        # mask = mask[y_min:y_max, x_min:x_max]
        save_face_shirt_path = dst_data_face_shirt_path + name +"/"
        os.mkdir(save_face_shirt_path)

        cv2.imwrite(save_face_shirt_path+"identify.jpg", face_hair)
        cv2.imwrite(save_face_shirt_path+"non_top_head.jpg", body_non_shirt)
        cv2.imwrite(save_face_shirt_path+"non_head_body.jpg", body_old_shirt)

        # cv2.imshow("mask_bg", mask_bg)
        # cv2.imshow("identify.jpg", face_hair)
        # cv2.imshow("non_top_head.jpg", body_non_shirt)
        # cv2.imshow("non_head_body.jpg", body_old_shirt)
        # cv2.waitKey()

        # top = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR) & image
        # top_crop = top[y_min:y_max, x_min:x_max]
        # top_crop = padding_resize(top_crop, 128)
        #
        # save_path = dst_path + name + "/"
        # os.mkdir(save_path)
        # for i in range(2000):
        #     try:
        #         top_transform = random_transform(top, 10, 20)
        #         y_nonzero, x_nonzero, _ = np.nonzero(top_transform)
        #         y_min = np.min(y_nonzero)
        #         y_max = np.max(y_nonzero)
        #         x_min = np.min(x_nonzero)
        #         x_max = np.max(x_nonzero)
        #
        #         top_transform = top_transform[y_min:y_max, x_min:x_max]
        #         top_transform = padding_resize(top_transform, 128)
        #
        #         # cv2.imshow("hjhj", top_transform)
        #         # cv2.imshow("hjhjhj", top_crop)
        #         # cv2.waitKey()
        #
        #         cv2.imwrite(save_path+str(i)+".jpg", top_transform)
        #         cv2.imwrite(dst_ori_shirt_path+name+".jpg", top_crop)
        #     except:
        #         pass
        count+=1
        print("Da xu ly: "+str(count)+", tong so: "+str(l))


# process(image_list)

threading.Thread(target=process, args=(image_list[0:400], )).start()
threading.Thread(target=process, args=(image_list[400:800], )).start()
threading.Thread(target=process, args=(image_list[800:1200], )).start()
threading.Thread(target=process, args=(image_list[1200:1600], )).start()
threading.Thread(target=process, args=(image_list[1600:2000], )).start()
threading.Thread(target=process, args=(image_list[2000:2400], )).start()
threading.Thread(target=process, args=(image_list[2400:2800], )).start()
threading.Thread(target=process, args=(image_list[2800:3200], )).start()
threading.Thread(target=process, args=(image_list[3200:3600], )).start()
threading.Thread(target=process, args=(image_list[3600:4000], )).start()
threading.Thread(target=process, args=(image_list[4000:4400], )).start()
threading.Thread(target=process, args=(image_list[4400:4800], )).start()
threading.Thread(target=process, args=(image_list[4800:5200], )).start()
threading.Thread(target=process, args=(image_list[5200:5600], )).start()
threading.Thread(target=process, args=(image_list[5600:6000], )).start()
threading.Thread(target=process, args=(image_list[6000:6400], )).start()
threading.Thread(target=process, args=(image_list[6400:6800], )).start()
threading.Thread(target=process, args=(image_list[6800:7200], )).start()
threading.Thread(target=process, args=(image_list[7200:7600], )).start()
threading.Thread(target=process, args=(image_list[7600:8000], )).start()
threading.Thread(target=process, args=(image_list[8000:8400], )).start()
threading.Thread(target=process, args=(image_list[8400:8800], )).start()
threading.Thread(target=process, args=(image_list[8800:9200], )).start()
threading.Thread(target=process, args=(image_list[9200:9600], )).start()
threading.Thread(target=process, args=(image_list[9600:10000], )).start()
threading.Thread(target=process, args=(image_list[10000:len(image_list)], )).start()