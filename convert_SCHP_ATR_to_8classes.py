import cv2
import os
import numpy as np
import threading


# RGB color format
SCHP_MAPPING = {
    'Background': (0, 0, 0),
    'Bag': (0, 64, 0),
    'Belt': (64, 0, 0),
    'Dress': (128, 128, 128),
    'Face': (192, 128, 0),
    'Hair': (0, 128, 0),
    'Hat': (128, 0, 0),
    'Left-arm': (64, 128, 128),
    'Left-leg': (64, 0, 128),
    'Left-shoe': (192, 0, 0),
    'Pants': (0, 128, 128),
    'Right-arm': (192, 128, 128),
    'Right-leg': (192, 0, 128),
    'Right-shoe': (64, 128, 0),
    'Scarf': (128, 64, 0),
    'Skirt': (128, 0, 128),
    'Sunglasses': (128, 128, 0),
    'Upper-clothes': (0, 0, 128)}

SCHP_TO_SMEX_MAPPING = {
    (0, 0, 0): 0,  # Background
    (0, 64, 0): 0,  # Bag => Background
    (64, 0, 0): 0,  # Belt => Background
    (128, 128, 128): 3,  # Dress => Top
    (192, 128, 0): 2,  # Face
    (0, 128, 0): 1,  # Hair
    (128, 0, 0): 1,  # Hat => Hair
    (64, 128, 128): 5,  # Left-arm => Right-arm
    (64, 0, 128): 8,  # Left-leg => Right-leg
    (192, 0, 0): 8,  # Left-shoe => Right-leg
    (0, 128, 128): 6,  # Pants => Bottoms
    (192, 128, 128): 4,  # Right-arm => Left-arm
    (192, 0, 128): 7,  # Right-leg => Left-leg
    (64, 128, 0): 7,  # Right-shoe => Left-leg
    (128, 64, 0): 0,  # Scarf => Background
    (128, 0, 128): 6,  # Skirt => Bottoms
    (128, 128, 0): 2,  # Sunglasses => Face
    (0, 0, 128): 3,  # Upper-clothes => Tops
}

# Background  # Hair     # Face       # Tops      # Left-arm
SMEX_LABEL_COLOUR = [(0, 0, 0), (255, 0, 0), (0, 0, 255), (0, 0, 85), (0, 255, 255),
                     # Right-arm       # Bottoms       # Left-leg         # Right-leg
                     (51, 170, 221), (0, 85, 85), (170, 255, 85), (85, 255, 170)]


def change_color_convention(images, source, destination, source_colors_to_classes, target_colors):
    """
    :param images:
    :param source:
    :param destination:
    :param source_colors_to_classes: RGB to class indices
    :param target_colors: list of RGB tuples (each index corresponds to class index)
    :return:
    """

    if not os.path.exists(destination):
        os.makedirs(destination)

    total = len(images)

    count = 1

    for image in images:
        schp_image = cv2.imread(os.path.join(source, image))
        schp_image = cv2.cvtColor(schp_image, cv2.COLOR_BGR2RGB)
        for color, value in source_colors_to_classes.items():
            schp_image[np.where((schp_image == color).all(axis=2))] = value

        for i in range(len(target_colors)):
            schp_image[np.where((schp_image == i).all(axis=2))] = target_colors[i]

        schp_image = cv2.cvtColor(schp_image, cv2.COLOR_RGB2BGR)

        cv2.imwrite(os.path.join(destination, image), schp_image)
        print("Process {} in {}".format(count, total))
        count += 1



if __name__ == '__main__':
    source = '/media/nhatdeptrai/DATA/lookbook/SCHP_segment'
    destination = '/media/nhatdeptrai/DATA/lookbook/test'

    images = os.listdir(source)

    threading.Thread(target=change_color_convention, args=(images[0:30000], source, destination, SCHP_TO_SMEX_MAPPING, SMEX_LABEL_COLOUR, )).start()
    threading.Thread(target=change_color_convention, args=(images[30000:50000], source, destination, SCHP_TO_SMEX_MAPPING, SMEX_LABEL_COLOUR, )).start()
    threading.Thread(target=change_color_convention, args=(images[50000:len(images)], source, destination, SCHP_TO_SMEX_MAPPING, SMEX_LABEL_COLOUR, )).start()
