import os
import cv2
import numpy as np
import random
import threading

BODY_SEGMENT_PATH = "/home/nhatdeptrai/Desktop/infer_parsing_data_train/8_body_parts_segments/"
BOTTOM_PATH = "/home/nhatdeptrai/Desktop/infer_parsing_data_train/clothes_segments/bottoms/"
TOP_PATH = "/home/nhatdeptrai/Desktop/infer_parsing_data_train/clothes_segments/tops/"
LABEL_PATH = "/home/nhatdeptrai/Desktop/infer_parsing_data_train/labels_8_classes/"
RANDOM_NUM = 10


def padding_resize(im, desired_size=128):
    old_size = im.shape[:2]  # old_size is in (height, width) format
    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    # new_size should be in (width, height) format

    im = cv2.resize(im, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    color = [0, 0, 0]
    new_im = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                value=color)
    return new_im


def random_transform(image, min_percentage, max_percentage):
    y_nonzero, x_nonzero, _ = np.nonzero(image)
    if x_nonzero.size != 0:
        x_min = np.min(x_nonzero)
        x_max = np.max(x_nonzero)
        y_min = np.min(y_nonzero)
        y_max = np.max(y_nonzero)

        left_bottom = [x_min, y_max]
        left_top = [x_min, y_min]
        right_top = [x_max, y_min]
        right_bottom = [x_max, y_max]

        x_avg = int((x_min + x_max) / 2)
        y_avg = int((y_min + y_max) / 2)

        points = np.array([left_bottom, left_top, right_top, right_bottom])

        dst_points = np.array([
            (left_bottom[0] + random.choice([-1, 1]) * random.randint(int(x_avg * min_percentage / 100),
                                                                      int(x_avg * max_percentage / 100)),
             left_bottom[1] + random.choice([-1, 1]) * random.randint(int(y_avg * min_percentage / 100),
                                                                      int(y_avg * max_percentage / 100))),
            (left_top[0] + random.choice([-1, 1]) * random.randint(int(x_avg * min_percentage / 100),
                                                                   int(x_avg * max_percentage / 100)),
             left_top[1] + random.choice([-1, 1]) * random.randint(int(y_avg * min_percentage / 100),
                                                                   int(y_avg * max_percentage / 100))),
            (right_top[0] + random.choice([-1, 1]) * random.randint(int(x_avg * min_percentage / 100),
                                                                    int(x_avg * max_percentage / 100)),
             right_top[1] + random.choice([-1, 1]) * random.randint(int(y_avg * min_percentage / 100),
                                                                    int(y_avg * max_percentage / 100))),
            (right_bottom[0] + random.choice([-1, 1]) * random.randint(int(x_avg * min_percentage / 100),
                                                                       int(x_avg * max_percentage / 100)),
             right_bottom[1] + random.choice([-1, 1]) * random.randint(int(y_avg * min_percentage / 100),
                                                                       int(y_avg * max_percentage / 100)))
        ])
        points = np.float32(points)
        dst_points = np.float32(dst_points)

        M = cv2.getPerspectiveTransform(points, dst_points)
    return M


def process(image_names):
    count = 0
    for name in image_names:
        bodyseg = cv2.imread(BODY_SEGMENT_PATH+name)
        bottom = cv2.imread(BOTTOM_PATH+name)
        top = cv2.imread(TOP_PATH+name)
        label = cv2.imread(LABEL_PATH+name)
        for i in range(RANDOM_NUM):

            #Lay ma tran bien doi cho bodySeg
            bodyseg_M = random_transform(bodyseg, 5, 10)

            #Ap dung chung 1 ma tran bien doi cho bodySeg va label
            bodyseg_transform = cv2.warpPerspective(bodyseg, bodyseg_M, (bodyseg.shape[1], bodyseg.shape[0]))
            label_transform = cv2.warpPerspective(label, bodyseg_M, (label.shape[1], label.shape[0]))

            #Tuong tu voi bottom
            bottom_M = random_transform(bottom, 2, 4)
            bottom_transform = cv2.warpPerspective(bottom, bottom_M, (bottom.shape[1], bottom.shape[0]))

            #Va top
            top_M = random_transform(top, 5, 10)
            top_transform = cv2.warpPerspective(top, top_M, (top.shape[1], top.shape[0]))

            # bodyseg_transform = padding_resize(bodyseg_transform, 256)
            # label_transform = padding_resize(label_transform, 256)
            # top_transform = padding_resize(top_transform, 256)
            # bottom_transform = padding_resize(bottom_transform, 256)

            cv2.imshow("bodyseg", bodyseg_transform)
            cv2.imshow("label", label_transform)
            cv2.imshow("top", top_transform)
            cv2.imshow("bottom", bottom_transform)
            cv2.waitKey()



image_list = os.listdir(BODY_SEGMENT_PATH)

for i in range(0, 1500, 100):
    threading.Thread(target=process, args=(image_list[i:i+100], )).start()
